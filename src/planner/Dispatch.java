package planner; /**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

import planner.reduce.Reduction;

import java.util.*;
/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-okt-31 : 13:46:11
 */
public class Dispatch {
    protected List<Cab> cabs;
    protected final Set<Passenger> waitingPassengers = new HashSet<Passenger>();
    protected final List<Passenger> passengers = new LinkedList<Passenger>();
    private final List<List<Estimate>> estimatedDriveTimes = new LinkedList<List<Estimate>>();
    private Reduction reduction;
    protected int maxNumberOfDrops = -1; // set to -1 for infinite
    protected int time = 0;

    public Dispatch(Reduction reduction, int maxNumberOfDrops) {
        this.reduction = reduction;
        this.maxNumberOfDrops = maxNumberOfDrops;
    }

    int i = 0;
    int totalFaresTendered = 0;

    public Dispatch() {
    }

    int maxQueueLength = 0;
    public void call(Passenger passenger) {
        // a passenger calls about getting picked up
        // send the queue of un-picked-up passengers out to tender for all the cabs.
        waitingPassengers.add(passenger);
        // todo: actually, it's going to be HELL estimating all these waiting times. lets just run analysis on the actual waiting time
/*
        // _todo: create different estimates for waiting time
        passenger.setWaitingTime(new WaitingTime(0,0,0,0,0,0));
        int n = 10;
        int totalSinceStart = 0;
        int avgSinceStart = 0;
        int maxSinceStart = 0;
        int totalLastNSteps = 0;
        int avgLastNSteps = 0;
        int maxLastNSteps = 0;
        int totalLastNCalls = 0;
        int avgLastNCalls = 0;
        int maxLastNCalls = 0;
        for (Passenger p : passengers) {
            totalSinceStart += p.getActualWaitingTime();
            if (p.getActualWaitingTime() > maxSinceStart) {
                maxSinceStart = p.getActualWaitingTime();
            }

        }
*/
//        System.out.println("   Passenger waiting at " + passenger.getPickupLocation());
        i++;
//        System.out.println(i + ":" + time + ": tendering with " + waitingPassengers.size() + " passengers waiting (and N cabs free)" + "avg queue length: " + (totalFaresTendered / i));
        totalFaresTendered += waitingPassengers.size();
        if (waitingPassengers.size() > maxQueueLength) {
            maxQueueLength = waitingPassengers.size();
        }
        tender();
    }

    public int getAverageQueueLength() {
        return (totalFaresTendered / Math.max(i,1));
    }

    public int getMaxQueueLength() {
        return maxQueueLength;
    }

    public boolean debug = false;
    private void tender() {
        // ask all the cabs about their estimates, if they are not busy. (map()):
        estimatedDriveTimes.clear();
        boolean estimationRequested = false;
        for (Cab cab : cabs) {
            if (debug) System.err.println(time + ": " + cab.getCabId() + " was" + (cab.isBusy() ? " busy":" NOT busy") + " and had " + (cab.getPassenger() == null ? "NO passenger set" : "a passenger set") + " and " + (cab.getDrops() < maxNumberOfDrops ? "had too many drops":"was OK with drops") );
            boolean cabIsBusy = cab.isBusy();
//            boolean dropsMatter = (maxNumberOfDrops != -1);
            boolean noDropsAllowed = maxNumberOfDrops == 0;
            boolean cabIsOkWithDrops = cab.getDrops() < maxNumberOfDrops;
            boolean cabHasPassenger = cab.getPassenger() != null;

//            if (dropsMatter) {
                if (cabIsBusy || (noDropsAllowed && cabHasPassenger)) {
                    continue;
                } else if (noDropsAllowed && !cabHasPassenger) {
                    cab.calculateDriveTimes(waitingPassengers);
                    estimationRequested = true;
                } else if (cabIsOkWithDrops) {
                    cab.calculateDriveTimes(waitingPassengers);
                    estimationRequested = true;
                } /*
                // we put this here to remember that we can't just break out of if-statements unless we have covered all the angles
                else if (!cabIsOkWithDrops) {
                    continue;
                }
                */
/*
            } else {
                cab.calculateDriveTimes(waitingPassengers);
                estimationRequested = true;
            }
*/            
            // note: since we are stepping in discrete time, we could just do this all linearly, since it is the reduce that takes time anyway
            // if the cab doesn't want to answer (like if he is busy), he'll register null as his drive time list
        }
        // since you can't be busy if you have a passenger, this means that we should always get cabs.size() - drivingToPickup answers at least
        reduce();
        if (estimationRequested) {

        } else {
            //System.out.println("no estimation requested");
        }
    }

    public void registerDriveTimes(List<Estimate> estimates) {
        // NOTE: we don't actually need synchronization until we spawn threads, but we might as well keep it for semantic reasons
        // NOTE: we need to make a list-of-lists, otherwise we won't know how many taxis to do this for
        estimatedDriveTimes.add(estimates);
    }

    private void reduce() {
        // determine the winner (reduce()/fold()):
        HashMap<Cab, Passenger> winningEstimates = reduction.reduce(estimatedDriveTimes);

        if (winningEstimates.size() == 0 && debug) {
            int free = 0;
            int drivingToPickup = 0;
            for (Cab cab : cabs) {
                if (!cab.isBusy()) {
                    free++;
                }
                if (cab.getPassenger() != null) {
                    drivingToPickup++;
                }
            }
            System.err.println(time + ": no winning estimates with " + estimatedDriveTimes.size() + " estimated drive times and " + waitingPassengers.size() + " passengers waiting with " + free + " cabs free and " + drivingToPickup + " out of " + cabs.size() + " driving to pickup with " + passengers.size() + " passengers served");
        }

        // since cabs can't be duplicated, they need to be the key.
        // cabs with passenger=null just stay where they are
        // this can cause passengers to be left without a ride, but they'll get serviced when there are available cabs
//        System.out.print(time + ": assigning drives:");
        for (Map.Entry<Cab, Passenger> entry : winningEstimates.entrySet()) {
//            System.out.print(entry.getKey().getCabId() + " to pick up " + entry.getValue().getId() + "; ");
            entry.getKey().pickup(entry.getValue());
            // don't remove the passenger as waiting until it has actually been picked up
        }
//        System.out.println("");
        for (Cab cab : cabs) {
            // for all cabs that were not in the list and are not busy, we need to remove any drives they currently have (how would this even happen, unless they were busy, in which case we can't remove anything)???
            // it only happens about 3% of the time, but it happens none the less
            if (!cab.isBusy() && cab.getPassenger() != null && !winningEstimates.keySet().contains(cab)) {
//                System.err.println(time + ": stopping cab " + cab.getCabId() + " from picking up " + cab.getPassenger() + " current queue length: " + waitingPassengers.size());
                // because it has been dispatched to someone else, and for unknown reasons, we're asking this cab to just stay put and do nothing.
                // this happens when we have enough free cars to service the needs in the queue
                cab.clearPickup();
            }
        }
    }

    public void pickedUpPassenger(Passenger p) {
        waitingPassengers.remove(p);
        if (passengers.contains(p)) {
            System.err.println("dropping of a passenger that already existed!: " + p.getId());
        }
        passengers.add(p);
    }

    public void registerCabs(List<Cab> cabs) {
        this.cabs = cabs;
    }

    public void tick() {
        time++;
        for (Passenger p : waitingPassengers) {
            p.tick();
        }
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }
}
