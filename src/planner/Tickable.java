package planner;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-nov-01 : 23:42:52
 */
public interface Tickable {
    public void tick(); // futhers time
}
