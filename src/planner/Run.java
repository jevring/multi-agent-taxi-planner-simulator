/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package planner;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-nov-07 : 00:33:17
 */
public class Run {
    public int avgQueueLength = 0;
    public int maxQueueLength = 0;
    public int minWaitingTime = 0;
    public int avgWaitingTime = 0;
    public int maxWaitingTime = 0;
    public double efficiency = 0d;

    public Run(int avgQueueLength, int maxQueueLength, int minWaitingTime, int avgWaitingTime, int maxWaitingTime, double efficiency) {
        this.avgQueueLength = avgQueueLength;
        this.maxQueueLength = maxQueueLength;
        this.minWaitingTime = minWaitingTime;
        this.avgWaitingTime = avgWaitingTime;
        this.maxWaitingTime = maxWaitingTime;
        this.efficiency = efficiency;
    }


    public Run() {
        
    }
}
