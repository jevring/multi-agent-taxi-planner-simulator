
/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package planner.reduce;

import planner.Estimate;
import planner.Cab;
import planner.Passenger;

import java.util.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-okt-31 : 14:31:40
 */
public class ShortestIndividualDistanceReduction implements Reduction {
    public HashMap<Cab, Passenger> reduce(List<List<Estimate>> estimates) {
        HashMap<Cab, Passenger> assignments = new HashMap<Cab, Passenger>();
        if (estimates.size() > 0) {
            TreeSet<Estimate> all = new TreeSet<Estimate>(new Comparator<Estimate>() {
                public int compare(Estimate o1, Estimate o2) {
                    if (o1.getTime() == o2.getTime()) {
                        return 0;
                    } else if (o1.getTime() < o2.getTime()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });
            for (List<Estimate> l : estimates) {
                all.addAll(l);
            }
            HashSet<Passenger> assignedPassengers = new HashSet<Passenger>();
            //System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++got " + estimates.size() + " estimates for this round");

            int numberOfPassengers = estimates.get(0).size();
            for (Estimate e : all) {
                if (!assignments.containsKey(e.getCab()) && !assignedPassengers.contains(e.getPassenger())) {
                    assignments.put(e.getCab(), e.getPassenger());
                    assignedPassengers.add(e.getPassenger());
                    // the only time this is updated in in here, so the check should happen here, to reduce the number of times it is checked.
                    if (assignedPassengers.size() == numberOfPassengers) {
                        // if we have assigned all bidding cars, exit
                        break;
                    }
                }
            }
        }

        return assignments;
    }
}
