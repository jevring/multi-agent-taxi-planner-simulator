package planner.reduce;

import planner.Estimate;
import planner.Cab;
import planner.Passenger;

import java.util.List;
import java.util.HashMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-okt-31 : 14:30:40
 */
public interface Reduction {
    public HashMap<Cab, Passenger> reduce(List<List<Estimate>> estimates);
}
