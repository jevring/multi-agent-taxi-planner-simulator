package planner; /**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

import planner.Location;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-okt-31 : 13:48:28
 */
public class Passenger implements Tickable {
    private static long id = 0L;
    private Location pickupLocation;
    private Location destination;
    private int time = 0;
    private long myId = 0L;
    private int timesDropped = 0;
    private int fareDistance;
    private int waitingTime = 0;
    private boolean pickedUp = false;

    public Passenger(City city) {
        id++; // every new passenger spawned gets a new id. thread safe (might cause overhead that we don't need)
        myId = id;
        pickupLocation = Location.random(city.getWidth(), city.getHeight());
        destination = Location.random(city.getWidth(), city.getHeight());
        fareDistance = (int)City.distance(pickupLocation, destination);
    }

    public Location getPickupLocation() {
        return pickupLocation;
    }

    public Location getDestination() {
        return destination;
    }

    public void tick() {
        time++;
        if (!pickedUp) {
            waitingTime++;
        }
    }
    public void pickup() {
        pickedUp = true;
    }

    public String toString() {
        return "[id=" + myId + ";from=" + pickupLocation + ";to=" + destination + "]";  
    }

    public int getTimesDropped() {
        return timesDropped;
    }

    public void drop() {
        timesDropped++;
    }

    public int getFareDistance() {
        return fareDistance;
    }
    public int getWaitingTime() {
        return waitingTime;
    }

    public long getId() {
        return myId;
    }


    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passenger passenger = (Passenger) o;

        if (myId != passenger.myId) return false;

        return true;
    }

    public int hashCode() {
        return (int) (myId ^ (myId >>> 32));
    }
}
