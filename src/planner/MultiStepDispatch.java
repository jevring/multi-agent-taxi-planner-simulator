/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package planner;

import java.util.*;


/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-nov-05 : 18:32:39
 */
public class MultiStepDispatch extends Dispatch {
    public MultiStepDispatch(int maxNumberOfDrops) {
        this.maxNumberOfDrops = maxNumberOfDrops;
    }


    public void call(Passenger passenger) {
        /*
        Why we don't need a multi-step planning algorithm:
        trigger on new call:
        - because we would trigger a re-run of the algorithm each time a new call comes in, the multi-step algorithm would be approximated by the single-step algorithm, especially for a high call-frequency
        - the multi-step algorithm only makes senase where call frequencies are low enough so that a car can handle on average 2 "steps" between each execution of the algorithm
        trigger on "end of path":
        - alternatively one could wait until all "paths" have been executed (driven) before re-calculating the paths, but then we run the risk of leting people wait for a long time before getting picked up, or we might miss an opportunity for an efficient delivery
        - or one could wait until the first cab finishes it's path, but that means waiting for all cabs, since ideally the paths should be of equal length for all the cabs, to ensure the best usage of cabs, and the fairest driving situation

         */
        
        /*
        todo: determine when to trigger a recalculation

        
        - permute the fares
        - calculate the cost by taking the cost of the fare vectors, and adding the cost for the inter-vector travel
        - select the one with the lowest cost
         */
    }

    private void findPaths() {
/*
        Set<Map<MultiStepCab, Queue<Passenger>>> permutedPassengers = permuteWaitingPassengers();
        TreeMap<Double, Map<MultiStepCab, Queue<Passenger>>> costMap = calculateCost(permutedPassengers);
        Map<MultiStepCab, Queue<Passenger>> cheapestSet = costMap.firstEntry().getValue(); // cheapest complete set of paths.
        for (Map.Entry<MultiStepCab, Queue<Passenger>> entry : cheapestSet.entrySet()) {
            entry.getKey().pickup(entry.getValue());
        }
*/        
    }

    private Set<Map<MultiStepCab, Queue<Passenger>>> permuteWaitingPassengers() {
        // permute all the waiting passengers into N (numberOfCabs) buckets

        // for each linear permutation of the passengers, create all possible permutations that segment this permutaion into N different parts
        // and those in turn need to be permuted between the buckets
        // this is WAY more than we bargained for


        return null;
    }
}
