package planner; /**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

import java.util.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-okt-31 : 13:46:17
 */
public class Cab implements Tickable {
    private static long id = 0L;
    private long cabId = 0L;
    protected Passenger passenger;
    private Location location;
    private boolean busy = false;
    private Dispatch dispatch;
    private int speed = 0;
    private int drops = 0;
    private int time = 0;

    private long distanceDriven = 0;
    private long distanceDrivenEmpty = 0;
    private long totalNumberOfDrops = 0;
    private List<Passenger> completedFares = new LinkedList<Passenger>();


    public Cab(Location location, Dispatch dispatch, int speed) {
        id++;
        cabId = id;
        this.location = location;
        this.dispatch = dispatch;
        this.speed = speed;
    }

    public void tick() {
        // invoked by the city as time passes
        time++;
        if (passenger != null) {
            // only move if we actually have soemthing to do
            if (busy) {
                // move towards the destination
                move(passenger.getDestination());
                checkDrop();
            } else {
                // move towards the pickup point
                move(passenger.getPickupLocation());
                checkPickup();
            }
        }
    }

    private void checkPickup() {
        if (location.equals(passenger.getPickupLocation())) {
            loadPassenger();
        }
    }

    private void checkDrop() {
        if (location.equals(passenger.getDestination())) {
            deliverPassenger();
        }
    }

    int overshootCounter = 0;
    private void move(Location goal) {
        if (location.equals(goal)) {
            return;
        }
        // we want to move 'speed' units towards 'location'
        // we need to round to actually hitable destinations
        // NOTE: if we don't get the granularity small enough, we will waste a lot of time overshooting our destinations!

//        System.out.print(cabId + ": move from " + this.location + " moving towards " + goal);
        double fullDeltaX = goal.x - location.x;
        double fullDeltaY = goal.y - location.y;
        double deltaX, deltaY;
        // ratio
        double distanceLeft = calculateDistanceTo(goal);
        double ratio = speed / distanceLeft;
        deltaX = fullDeltaX * ratio;
        deltaY = fullDeltaY * ratio;
        //System.out.println("RATIO: deltaX = " + deltaX + "; deltaY = " + deltaY);
/*
        // angle
        double angle = Math.atan(fullDeltaX / fullDeltaY);
        deltaX = speed * Math.sin(angle);
        deltaY = speed * Math.cos(angle);
        System.out.println("ANGLE: deltaX = " + deltaX + "; deltaY = " + deltaY);
*/
//        System.out.print(" which is a distance of " + distanceLeft + " of which we have traveled " + speed);
        double distanceTravelledInThisStep;
        if (ratio > 1) {
            this.location = goal;
            overshootCounter++;
//            System.out.println("+++++++++++++++++++++++++++++++++++" + overshootCounter + ": overshooting our goal by a factor of " + ratio);
            distanceTravelledInThisStep = speed / ratio;
        } else {
            this.location.x += deltaX;
            this.location.y += deltaY;
            distanceTravelledInThisStep = speed;
        }
        distanceDriven += distanceTravelledInThisStep;
        if (!busy) {
            distanceDrivenEmpty += distanceTravelledInThisStep;
        }
//        System.out.println(" and ended up at " + this.location);

    }

    /*
   Abstract time in steps, so no real time is necessary. waiting time is measured in tick()s from when the call came in
    */


    public void pickup(Passenger p) {
//        System.out.println(cabId + ": Pickup:  " + p);
        // this method is called by the dispatch when an optimal passenger has been determined
        if (passenger != null && !passenger.equals(p)) {
            drops++;
            totalNumberOfDrops++;
            passenger.drop();
//            System.out.println(cabId + ": Abandon: " + passenger + " in favor of " + p);
        } // if not, then we were just standing still, or already handing this particular passenger
        passenger = p;
    }

    private void deliverPassenger() {
//        System.out.println(cabId + ": Deliver: " + this.passenger);
        completedFares.add(passenger); // this way we can get all the statistics from the cabs themselves at the end
        passenger = null;
        busy = false;
    }

    private void loadPassenger() {
        drops = 0;
        busy = true;
        dispatch.pickedUpPassenger(passenger);
        passenger.pickup(); // terminates the waiting time for the passenger
//        System.out.println(time + ":" + cabId + ": Loaded:  " + passenger);
    }

    public void calculateDriveTimes(Set<Passenger> passengers) {
        // this chould spawn a thread (or wake the current one) to do the calculations
        // No, IF this was real, we would spawn threads here, but since it's not, and we only have one core anyway, we'll stick to doing it in a linear fashion.
//        if (!busy) {
            List<Estimate> estimates = new LinkedList<Estimate>();
            //List<Estimate> estimates = new LinkedList<Estimate>();
            for (Passenger p : passengers) {
                estimates.add(new Estimate(calculateDistanceTo(p.getPickupLocation()), p, this));
            }
            dispatch.registerDriveTimes(estimates);
//        } 
    }

    private double calculateDistanceTo(Location destination) {
        return City.distance(this.location, destination);
    }

    public boolean isBusy() {
        return busy;
    }

    public int getDrops() {
        if (drops > 0 && passenger == null) {
            System.out.println(time + ": cab " + cabId + " had more than 0 drops, but no passenger!");
        }
        return drops;
    }

    public int getSpeed() {
        return speed;
    }

    public int getTime() {
        return time;
    }

    public long getDistanceDriven() {
        return distanceDriven;
    }

    public long getTotalNumberOfDrops() {
        return totalNumberOfDrops;
    }

    public List<Passenger> getCompletedFares() {
        return completedFares;
    }

    public long getCabId() {
        return cabId;
    }

    public long getDistanceDrivenEmpty() {
        return distanceDrivenEmpty;
    }

    public void clearPickup() {
        this.passenger = null;
        this.drops = 0;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public static void reset() {
        id = 0;
    }
}
