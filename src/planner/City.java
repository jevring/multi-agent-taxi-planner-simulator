package planner;

import planner.reduce.ShortestIndividualDistanceReduction;

import java.util.List;
import java.util.LinkedList;
import java.io.PrintStream;
import java.io.File;
import java.io.FileNotFoundException;
/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-okt-31 : 13:56:46
 */
public class City {
    private final List<Cab> cabs = new LinkedList<Cab>();
    private int height = 0;
    private int width = 0;
    private int cabSpeed = 0;
    private Dispatch dispatch;
    private long time = 0;
    private int callRate = 5;
    private int callsMade = 0;
    private int numberOfCallsBeforeQuitting = -1;
    private int numberOfStepsBeforeQuitting = -1;

    public City(int width, int height, int callRate, int cabSpeed, int numberOfStepsBeforeQuitting, int numberOfCallsBeforeQuitting) {
        if (cabSpeed < 2) {
            throw new IllegalArgumentException("cabSpeed needs to be higher than 1, otherwise the cabs can't move!");
        }
        this.width = width;
        this.height = height;
        this.callRate = callRate;
        this.cabSpeed = cabSpeed;
        this.numberOfStepsBeforeQuitting = numberOfStepsBeforeQuitting;
        this.numberOfCallsBeforeQuitting = numberOfCallsBeforeQuitting;
    }

    private void createCabs(int nCabs) {
        for (int i = 0; i < nCabs; i++) {
            // spawns cabs
            cabs.add(new Cab(Location.random(width, height), dispatch, cabSpeed));
            // todo: we should save the spawning script for the taxies and the calls, so that the run can be repeated
        }
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public List<Cab> getCabs() {
        return cabs;
    }

    public void setDispatch(Dispatch dispatch) {
        this.dispatch = dispatch;
    }

    public static double distance(Location source, Location destination) {
        return Math.sqrt(Math.pow(destination.x - source.x, 2) + Math.pow(destination.y - source.y,  2));
    }

        private boolean shouldQuit() {
        if (numberOfCallsBeforeQuitting > 0 && dispatch.getPassengers().size() >= numberOfCallsBeforeQuitting) {
            return true;
        } else if (numberOfStepsBeforeQuitting > 0 && numberOfStepsBeforeQuitting == time) {
            return true;
        } else {
            return false;
        }
    }

    public void run() {
        if (numberOfCallsBeforeQuitting == -1 && numberOfStepsBeforeQuitting == -1) {
            System.err.println("must set a number of calls or steps to run for");
            System.exit(-1);
        }
        // spawn travellers at a variable interval(or acording to a timed list of values, for replayability) that call the dispatch
        callsMade = 0;
        while (!shouldQuit()) {
            advanceTime();
            if (time % callRate == 0) {
            //if (Math.random() < 0.05) {

                callsMade++;
                dispatch.call(new Passenger(this));
            }
        }
    }
    private void advanceTime() {
/*        ONLY APPLICABLE WITH CALLS = 1000
        if (time > 20240) {
            dispatch.debug = true;
        }
        if (time > 25000) {
            throw new IllegalStateException("Something went wrong!");
        }
*/
        time++;
        for (Tickable t : cabs) {
            t.tick();
        }
        dispatch.tick();
    }

    private Run statistics(boolean simple) {
        /*
        CONCLUSIONS: 
        - as the number of cabs go up, the number of waiting passengers goes down (DUH)
        - as the speed of the cabs go up, the number of handled calls approaches the number of received calls (handled sometimes go over received, how is that even possible?

        - as the numberOfDropsAllowed variable shrinks, the waiting time rises exponentially (ny an order or magnitude or more)
         */
        /*
         - find a number of cabs to city size ratio that won't keep the queue from constantly expanding
         - measure: waiting time, efficiency
         - input: call rate, cabs, drops, city size/speed


         */

        if (!simple) {
            System.out.println("City size: " + new Location(width, height));
            System.out.println("Cabs: " + getCabs().size());
            System.out.println("Speed: " + cabSpeed);
            System.out.println("A new call is generated every " + callRate + " steps");
            System.out.println("Ending conditions: ");
            System.out.println("Steps: " + numberOfStepsBeforeQuitting);
            System.out.println("Calls: " + numberOfCallsBeforeQuitting);
            System.out.println("Calls (handled / received): " + dispatch.getPassengers().size() + "/" + callsMade); // this is the coverage data for how many cars are needed to service a certain call-rate and drive speed
            System.out.println("Time taken: " + time);
        }

        double efficiency = 0d;
        for (Cab cab : cabs) {
            //System.out.print("; drops (avg/max/total): " + (numberOfDrops / cab.getCompletedFares().size()) + "/" + mostDrops + "/" + cab.getTotalNumberOfDrops());
            // NOTE: we can't calculate the avg number of drops here, since a passenger that has been dropped by someone might be undropped by someone else, thus this weird line:
            // cab: 8; fares: 77; distance driven: 28824; drops (avg/max/total): 0/1/0; fare length (min/avg/max): 38/306/672

            int totalFareLength = 0;
            int longestFare = 0;
            int shortestFare = Integer.MAX_VALUE;
            for (Passenger p : cab.getCompletedFares()) {
                totalFareLength += p.getFareDistance();
                if (p.getFareDistance() > longestFare) {
                    longestFare = p.getFareDistance();
                }
                if (p.getFareDistance() < shortestFare) {
                    shortestFare = p.getFareDistance();
                }
            }
            String avgDrops = Double.toString(((double)cab.getTotalNumberOfDrops() / (double)cab.getCompletedFares().size()));
            double d = 1.0d - ((double)cab.getDistanceDrivenEmpty() / (double)cab.getDistanceDriven());
            String distanceRatio = Double.toString(d);
            efficiency += d;

            if (!simple) {
                System.out.println(
                        String.format("cab: %-3d fares: %-4d distance driven/empty: %8d / %-7d (%5s) drops(avg/total): %5s / %-3d fare length (min/avg/max):%5d/%5d/%5d",
                                cab.getCabId(),
                                cab.getCompletedFares().size(),
                                cab.getDistanceDriven(),
                                cab.getDistanceDrivenEmpty(),
                                distanceRatio.substring(0,Math.min(5,distanceRatio.length())),
                                avgDrops.substring(0,Math.min(5,avgDrops.length())),
                                cab.getTotalNumberOfDrops(),
                                (shortestFare == Integer.MAX_VALUE ? 0 : shortestFare),
                                (totalFareLength / Math.max(cab.getCompletedFares().size(),1)),
                                longestFare
                        )
                );
            }

        }
        int totalWaitingTime = 0;
        int minWaitingTime = Integer.MAX_VALUE;
        int maxWaitingTime = 0;
        for (Passenger p : dispatch.getPassengers()) {
            //System.out.println(p.getWaitingTime());
            if (p.getWaitingTime() > maxWaitingTime) {
                maxWaitingTime = p.getWaitingTime();
            }
            if (p.getWaitingTime() < minWaitingTime) {
                minWaitingTime = p.getWaitingTime();
            }
            totalWaitingTime += p.getWaitingTime();
        }
        if (!simple) {
            System.out.println("Waiting time: (min/avg/max): " + minWaitingTime + "/" + (totalWaitingTime / dispatch.getPassengers().size()) + "/" + maxWaitingTime);
            System.out.println("Queue length (avg/max): " + dispatch.getAverageQueueLength() + "/" + dispatch.getMaxQueueLength());
            String s = Double.toString((efficiency / cabs.size()));
            System.out.println("Efficiency average: " + s.substring(0, Math.min(s.length(), 5)));
        } else {
            System.out.print(minWaitingTime + "," + (totalWaitingTime / dispatch.getPassengers().size()) + "," + maxWaitingTime);
            System.out.print("," + dispatch.getAverageQueueLength() + "," + dispatch.getMaxQueueLength());
            String s = Double.toString((efficiency / cabs.size()));
            System.out.println("," + s.substring(0, Math.min(s.length(), 5)));
        }

        // print the different estimates the customer was given (6)
        // print the actual waiting time
        // give bigger stats on these (some kind of aggregate, variance and standard deviation)
        return new Run(dispatch.getAverageQueueLength(), dispatch.getMaxQueueLength(), minWaitingTime, (totalWaitingTime / dispatch.getPassengers().size()), maxWaitingTime, (efficiency / cabs.size()));

    }

    public static void main(String[] args) {
        // NOTE: the taxi speed has VERY little to do with the number of overshot targets (3-150 => 148-250 overshootings in 30000 steps)
        // regardless of if the call rate is high or low
        // it WILL, however, cause more and more calls to be handled in the given time frame, and the queues to be SIGNIFICANTLY shorter
        // the queue length should be offsettable by the number of cabs though
        // going from 9 to 20 cabs decreases queue length to 1/20

        // We note that the queue length goes up as the time progresses if we don't have enough cabs or high enough speed

        // NOTE: a certain car speed means that we'll have a certain queue lengthand subsequently a certain ratio of handled to received
        // todo: the cab speed OR city size MUST be static for all our tests, otherwise it won't make any sense!


/*
        try {
            File f = new File("waiting_time_to_drop_rate8-INF.csv");
            if (f.exists()) {
                throw new IllegalStateException("DO NOT OVERWRITE YOUR TEST DATA!");
            }
            System.setOut(new PrintStream(f));

            System.setErr(new PrintStream(new File("stderr.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
*/

        // todo: show that even when you allow for infinite drops, there is still a certain number of cabs needed to service a certain call rate (graph cabs and call rate against each other)
        // witdh, height, callRate, cabSpeed, numberOfStepsBeforeQuitting, numberOfCallsBeforeQuitting, numberOfAllowedDrops, numberOfCabs
        //runSimulation(640, 480, 20, 3, -1, 5000, Integer.MAX_VALUE, 15, false);
        queueLengthToWaitingTimes();
        // todo: it turns out that once you get over a certain amount of cabs (below a certain waiting time), then the number of drops doesn't matter

    }
    private static void dropsToQLandWT() {
        // todo: we can show that in a situation that would normally result in high waiting times, increasing the drops can help
        // todo: increasing the drops also leads to increased efficiency average
        // todo: note that this is NOT the case. increased drops leads to decreased efficiency
        // todo: we should combine this into the same graph (one line for wait time, and one for efficiency)
        for (int j = 8; j < 15; j++) {
            List<Run> runs = new LinkedList<Run>();
            System.out.println("waiting time min,waiting time average,waiting time max,queue length average,queue length max,efficiency average");
            for (int i = 0; i < 250; i++) {
                Run run = runSimulation(640, 480, 20, 3, -1, 5000, j, 9, true);
                runs.add(run);
            }
            System.out.println(",,,,,,Average");
            System.out.println(",,,,,,StandardDeviation");
            System.out.println(",,,,,," + j);
            //printAverageStats(runs);
            //System.out.println(j);
        }
        // NOTE: Integer.MAX_VALUE represents not caring about the number of drops
        List<Run> runs = new LinkedList<Run>();
        System.out.println("waiting time min,waiting time average,waiting time max,queue length average,queue length max,efficiency average");
        for (int i = 0; i < 250; i++) {
            Run run = runSimulation(640, 480, 20, 3, -1, 5000, Integer.MAX_VALUE, 9, true);
            runs.add(run);
        }
        System.out.println(",,,,,,Average");
        System.out.println(",,,,,,StandardDeviation");
        System.out.println(",,,,,," + Integer.MAX_VALUE);
        //printAverageStats(runs);
        //System.out.println(Integer.MAX_VALUE);

    }
    private static void queueLengthToWaitingTimes() {
        // hmm, we're getting VERY weird numbers for queue length. it varies like nobody's business.
        // _todo: we must do averages for each set of runs. Maybe do 20 runs per configuration setting, and take averages from there
        // _todo: use this to show that waiting time rises and falls with queue length
        // NOTE: this shows that queue length and waiting time isn't dependent on anything else at these levels (maybe it automatically falls at higher levels of something)
        System.out.println("waiting time min,waiting time average,waiting time max,queue length average,queue length max,efficiency average");
        for (int j = 28; j <= 40; j+=2) {
            for (int i = 0; i < 25; i++) {
                runSimulation(640, 480, 20, 3, -1, 5000, 0, j, true);
            }
            System.out.println(",,,,,," + j + " cabs, 0 drops");
        }
        System.out.println("waiting time min,waiting time average,waiting time max,queue length average,queue length max,efficiency average");
        for (int j = 28; j <= 40; j+=2) {
            for (int i = 0; i < 25; i++) {
                runSimulation(640, 480, 20, 3, -1, 5000, Integer.MAX_VALUE, j, true);
            }
            System.out.println(",,,,,," + j + " cabs, INF drops");
        }
        //todo: it would seem that the drops matter in some cases, but when we don't have enough cabs to service the population efficiently anyway, it doesn't matter
        //todo: it turns out that the fewer cabs there are, the higher the efficiency rate will be, regardless of the allowed drops. Also, even when allowed drops in INFINITE, the average drop rate is VERY small
        //todo: maybe we should deliver an average on the average drop rate
    }

    private static void printAverageStats(List<Run> runs) {
        Run avg = new Run();
        for (Run run : runs) {
            avg.avgQueueLength += run.avgQueueLength;
            avg.maxQueueLength += run.maxQueueLength;
            avg.minWaitingTime += run.minWaitingTime;
            avg.avgWaitingTime += run.avgWaitingTime;
            avg.maxWaitingTime += run.maxWaitingTime;
            avg.efficiency += run.efficiency;
        }
        avg.avgQueueLength = avg.avgQueueLength / runs.size();
        avg.maxQueueLength = avg.maxQueueLength / runs.size();
        avg.minWaitingTime = avg.minWaitingTime / runs.size();
        avg.avgWaitingTime = avg.avgWaitingTime / runs.size();
        avg.maxWaitingTime = avg.maxWaitingTime / runs.size();
        avg.efficiency = avg.efficiency / runs.size();
        System.out.println("waiting time min;waiting time average;waiting time max;queue length average;queue length max;efficiency average");
        System.out.println(avg.minWaitingTime + ";" + avg.avgWaitingTime + ";" + avg.maxWaitingTime + ";" + avg.avgQueueLength + ";" + avg.maxQueueLength + ";" + avg.efficiency);

    }

    private static Run runSimulation(int cityWidth, int cityHeight, int callRate, int cabSpeed, int numberOfStepsBeforeQuitting, int numberOfCallsBeforeQuitting, int maxNumberOfAllowedDrops, int numberOfCabs, boolean simple) {
        City city = new City(cityWidth, cityHeight, callRate, cabSpeed, numberOfStepsBeforeQuitting, numberOfCallsBeforeQuitting);
        Dispatch dispatch = new Dispatch(new ShortestIndividualDistanceReduction(), maxNumberOfAllowedDrops); // when using City(640, 480, 20, 3, -1, 1000), then anything >4 keeps the waiting times and the queue length down
        city.setDispatch(dispatch);
        city.createCabs(numberOfCabs);
        dispatch.registerCabs(city.getCabs());
        city.run();
        Run run = city.statistics(simple);
        Cab.reset();
        return run;
    }




}
